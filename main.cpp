/* ############################################
 * File: main.cpp
 * Author: Gael Marcadet
 * Description: Main
 * ########################################### */


#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <mpi.h>
#include "mpis.h"

#define IN
#define OUT

void prefix_sum( IN int val, IN MPI_Comm comm, OUT int *prefix, OUT int *size ) {
    // initialize mpi data in processor context
    int pid = 0;
    int nprocs = 0;
    MPI_Comm_rank( comm, &pid );
    MPI_Comm_size( comm, &nprocs );

    // to compute prefix sum, a processor communicates with there neighboors
    // chosen depending on current iteration index (called i). offset is equals to 2^i
    // If we suppose that tagerted neighboors exist, the previous send his locally computed value,
    // added to own precessor value and the result is send to the next neighboor.
    // We repeat until that's all processors cannot communicate with any other processors
    int offset = 1;
    int current_val = val;
    bool valid_index = true;
    while ( valid_index ) {
        valid_index = false;
        int next_value = current_val;

        // receive value from targeted previous processor
        int sender = pid - offset;
        if ( 0 <= sender ) {
            valid_index = true;

            int sent_value = 0;
            MPI_Recv( &sent_value, 1, MPI_INT, sender, 10, comm, MPI_STATUS_IGNORE );
            next_value += sent_value;
        }

        // send current value to targeted next processor.
        int recipient = pid + offset;
        if ( recipient < nprocs ) {
            valid_index = true;
            MPI_Ssend( &current_val, 1, MPI_INT, recipient, 10, comm );
        }
        
        // updating targeted neighboors and computed value
        offset = offset << 1;
        current_val = next_value;
    }

    // sharing size
    (*size) = pid == nprocs - 1 ? current_val : 0;
    MPI_Bcast( size, 1, MPI_INT, nprocs - 1, comm );

    // final step is to do an offset of each value
    if ( pid == 0 ) {
        MPI_Ssend( &current_val, 1, MPI_INT, 1, 10, comm );
        current_val = 0;
    } else if ( pid == nprocs - 1 ) {
        MPI_Recv( &current_val, 1, MPI_INT, pid - 1, 10, comm, MPI_STATUS_IGNORE );
    } else {
        int previous = pid - 1;
        int next = pid + 1;
        MPI_Sendrecv_replace( &current_val, 1, MPI_INT, next, 10, previous, 10, comm, MPI_STATUS_IGNORE );
    }

    // explodes
    (*prefix) = current_val;
}

void local_to_global( int local_index, int n, MPI_Comm comm, int * global_index ) {
    int pid = 0;
    int nprocs = 0;
    MPI_Comm_rank( comm, &pid );
    MPI_Comm_size( comm, &nprocs );
    
    int local_size = n / nprocs;
    int remain = n % nprocs;
    if ( pid < remain ) {
        (*global_index) = pid * (local_size + 1) + local_index;
    } else {
        (*global_index) = remain * (local_size + 1) + (pid - remain) * local_size + local_index;
    }
}


void swap( int * a, int * b ) {
    int tmp = *a;
    (*a) = (*b);
    (*b) = tmp;
}

void display_array( int * array, int n ) {
    using namespace std;
    for ( int i = 0; i < n; ++i ) {
        cout << array[i] << " ";
    }
    cout << endl;
}

void quicksort_sequential( int * array, int q, int r ) {
    if ( q < r ){
        int s = q;
        int pivot = array[q];
        for ( int i = q + 1; i < r; ++i ) {
            if ( array[i] <= pivot ) {
                s++;
                swap( array + s, array + i );
            }
        } 
        swap( array + q, array + s );
        //quicksort_sequential( array, pivot, q, s );
        //quicksort_sequential( array, pivot, s + 1, r );
    } 
}


void quicksort_sequential( int * array, int pivot, int q, int r ) {
    if ( q < r ){
        int s = q;
        for ( int i = q; i < r; ++i ) {
            if ( array[i] <= pivot ) {
                s++;
                swap( array + s, array + i );
            }
        } 
        //swap( array + q, array + s );
        //quicksort_sequential( array, pivot, q, s );
        //quicksort_sequential( array, pivot, s + 1, r );
    } 
}



void repartition( int subarray_size, int array_size, MPI_Comm comm, int * nprocs_on_task ) {
    int nprocs = 0;
    MPI_Comm_size( comm, &nprocs );

    if ( array_size < subarray_size ) {
        (*nprocs_on_task) = nprocs;
        return;
    }

    float subarray_percent = ( subarray_size * 100 ) / array_size;
    float proc_cover_range = 100 / nprocs;
    int nprocs_affected = 0;
    for ( int pid = 0; pid < nprocs; ++pid ) {
        if ( pid * proc_cover_range < subarray_percent ) {
            nprocs_affected++;
        } else {
            break;
        }
    }

    (*nprocs_on_task) = nprocs_affected;
}


void global_to_proc( int global_index, int n, MPI_Comm comm, int * designated_proc ) {
    int nprocs = 0;
    MPI_Comm_size( comm, &nprocs );

    int local_size = n / nprocs;
    int remain = n % nprocs;
    int receiver = 0;
    for ( int pid = 1; pid < nprocs; ++pid ) {
        int start_index = 0;
        if ( pid < remain ) {
            start_index = pid * (local_size + 1);
        } else {
            start_index = remain * (local_size + 1) + (pid - remain) * local_size;
        }
        if ( global_index < start_index ) {
            break;
        } else {
            receiver++;
        }
    }
    (*designated_proc) = receiver;
}


void sort_pivot( IN int * array, IN int n, IN int pivot, OUT int ** final_array, OUT int * sep_index ) {
    // the first step is to separates lower or equals values from array, compared with pivot
    // then we stores these values in res
    int * res = new int[n];
    int index = 0;
    for ( int i = 0; i < n; ++i ) {
        if ( array[i] <= pivot ) {
            res[index] = array[i];
            index++;
        }
    }

    // the last step is to store higher than pivot values in res array
    for ( int i = 0; i < n; ++i  ) {
        if ( pivot < array[i] ) {
            res[index] = array[i];
            index++;
        }
    }

    (*sep_index) = index;
    (*final_array) = res;
}

const int LEFT_COMM = 0;
const int RIGHT_COMM = 1;




void share_array( IN int * array, IN int n, IN int root, IN MPI_Comm comm, OUT int ** local_array_ptr, OUT int * local_size_ptr ) {
    // computing procs number in specified communicator
    int nprocs = 0;
    MPI_Comm_size( comm, &nprocs );

    // computing data size for each processor and displacements in initial array
    int * sizes = MPIS_Scatterv_sizes( n, nprocs );
    int * displs = MPIS_Scatterv_displacements( sizes, nprocs );

    // computing pid in specified communicator 
    int pid = 0;
    MPI_Comm_rank( comm, &pid );

    // sharing data for each processor
    int local_size = sizes[pid];
    int * local_array = new int[ local_size ];
    MPI_Scatterv( array, sizes, displs, MPI_INT, local_array, local_size, MPI_INT, root, comm );

    (*local_size_ptr) = local_size;
    (*local_array_ptr) = local_array;
}




int main( int argc, char ** args ) {
    int nprocs = 0;
    int pid = 0;
    MPI_Init( &argc, &args );
    MPI_Comm_rank( MPI_COMM_WORLD, &pid );
    MPI_Comm_size( MPI_COMM_WORLD, &nprocs );

    

    MPI_Finalize();
    return EXIT_SUCCESS;
}