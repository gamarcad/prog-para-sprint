#ifndef MPI_SUPPORT
#define MPI_SUPPORT

#define IN
#define OUT

#include <mpi.h>

/**
 * Split and send data from root processor, to each other.
 */
void MPIS_Scatterv( IN int root, IN int global_n, IN const void *in, OUT void *out, MPI_Datatype datatype, MPI_Comm comm );

/**
 * Gather piece of data from all processors and send result to root processor.
 */ 
void MPIS_Gatherv( int datalength, const void *sendbuf, MPI_Datatype datatype, void *recvbuf, int root, MPI_Comm comm );

/**
 * Quick start for MPI.
 */
void MPIS_Quick_start( IN int *argc, IN char ***argv, OUT int *pid, OUT int *nprocs  );

/**
 * Returns {@code true} if the pid has the first rank, else {@code false} otherwise.
 */
int MPIS_First( int pid );

/**
 * Returns {@code true} if the pid has the last rank, else {@code false} otherwise.
 */
int MPIS_Last( int pid );

/**
 * Returns the next rank of the specified pid.
 * If pid is the last of commmutator, then the returned pid is the first.
 */
int MPIS_Next( int pid );

/**
 * Returns the previous rank of the specified pid.
 * If pid is the first of commmutator, then the returned pid is the last.
 */
int MPIS_Previous( int pid );

/**
 * Returns the global index from data length and local index.
 */
int MPIS_Global_index( int n, int local_index );

/**
 * Returns the local index from data length and global index.
 */
int MPIS_Local_index( int n, int global_index );

/**
 * Returns the pid owner in charge of the local index
 */
int MPIS_Target_pid( int n, int global_index );


/**
 * Returns number of processors.
 */
int MPIS_NProcs();

/**
 * Returns the current pid of this function caller.s
 */
int MPIS_Pid();

/**
 * Computes displacements of data for each processors.
 */
int * MPIS_Scatterv_displacements( int * data_size, int nprocs );

/**
 * Computes size of data in charge for each processors.
 */
int * MPIS_Scatterv_sizes( int nval, int nprocs );


#endif